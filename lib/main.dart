import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'get lachst',
      theme: ThemeData(
        primarySwatch: Colors.pink,
      ),
      home: const Scaffold(
        body: MyHomePage(),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  bool _clicked = false;

  void _clickButton() {
    setState(() {
      _clicked = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: _clicked
            ? Image.asset(
                "images/lachs.jpg",
                fit: BoxFit.fill,
                alignment: Alignment.center,
              )
            : Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  IconButton(
                    icon: Image.asset("images/arcade_button.png"),
                    iconSize: 200,
                    onPressed: _clickButton,
                  ),
                ],
              ),
      ),
    );
  }
}
